---
layout: default
---

## Courses and workshops given

### Introduction to Linux terminal 2017
As a part of the activities organized by Linux UPC, when a new course starts we used to organize Linux install parties and introductory workshops.
At 2018, I was the speaker of the workshop, for which I created some new material. Given the success at the FIB it was also done at ETSETB.

_Linux, material, speaker_

[Link to material [es]](https://github.com/IEEE-BCN-SB/linux-terminal-workshop)

### Python introductory course
This was a free course organized by IEEE BCN SB in collaboration with LinucUPC held in 2017 and 2018.
For the later edition a jupyter notebook was created with theory and problems (some material was lost).

_Python, Jupyter notebook, material, speaker_

[Link to notebooks [es]](https://github.com/IEEE-BCN-SB/python-course)

## Projects

### IEEE BCN SB website
Website of the IEEE BCN (UPC) Student branch. All the backend is from scratch, but the backend is from a template.

_Python, Django_

[Link to repo](https://github.com/fevsea/IeeeUpcWeb)
|
[Website](http://ieee.upc.edu)


### Blockly robot
It's a rober-like robot powered by a Raspberry Pi.
The interesting part of the project is that can be programmed from a website,
no need to compile or connect cables. The code can be written in Python, JS or through blocks using the Blockly library.


 ![alt text](https://thumb.ibb.co/h9U2Wn/photo_2018_04_10_20_29_49.jpg)

_Python, Raspberry Pi, sockets, JS, Blockly_

[Link to repo](https://github.com/fevsea/blockly_robt)

### planeTracer
The objective of this project is pull dump1090 for air traffic and store it in a database.

_Python, SDR, SQL_

[Link to repo](https://github.com/fevsea/planeTracer)

## Hackathons

### uCode (UniZar) 2018

Android instant app that shows product info based on a NFC tag.
The data is obtained through a scrapper.

_Python, Java, Android, Django, DjangoRestFramework, BeautifulSoup, Retrofit_

[Link to repo](https://github.com/IEEE-BCN-SB/uCode)
|
[Presentation [es]](https://docs.google.com/presentation/d/1-BCeal05KQ9Jy7fsLAOdLrWWHXRo1JFt3rG3fmLXaKU/edit?usp=sharing)

### UDG Local Hack Day 2017
Password-based door access control with Arduino and a REST API to authentication.

_Python, Django, DjangoRestFramework, OpenAPI, Arduino, C_

[Link to repo](https://github.com/fevsea/udg)

### Lauzhack 2017
Android app that consumes a REST API to show a list with all the missions of ESA.

_Python, Java, Android, Django, DjangoRestFramework, BeautifulSoup, Retrofit_

[Link to repo](https://github.com/fevsea/lauzhack17)
|
[Link to devpost](https://devpost.com/software/lauzhack17-7r1jct)
